import React, { useState, useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { ThemeProvider } from "styled-components";
// import custom components
import { Body, Stars, Footer, Container, Header } from "./components";
// import views
import { Contact, Portfolio } from "./views";
// import Themes
import { lightTheme, spaceTheme, darkTheme } from "./theme/themes";

let useTheme = "";

const Portfolio2020 = () => {
  const [localTheme, setLocalTheme] = useState(localStorage.getItem("theme"));
  const [currentTheme, setCurrentTheme] = useState(lightTheme);

  // theme
  const changeTheme = (theme) => {
    switch (theme) {
      case "space":
        useTheme = spaceTheme;
        break;
      case "light":
        useTheme = lightTheme;
        break;
      case "dark":
        useTheme = darkTheme;
        break;
      default:
        useTheme = spaceTheme;
    }
    setCurrentTheme(useTheme);
  };

  // change theme if localtheme is available
  useEffect(() => {
    if (!localTheme) return;
    changeTheme(localTheme);
  }, [localTheme]);

  return (
    <ThemeProvider theme={currentTheme}>
        <Body>
          {currentTheme === spaceTheme && <Stars />}
          <BrowserRouter> 
            <Container>
              <Header setLocalTheme={setLocalTheme} />
              <Routes>
                <Route path="/" element={<Contact />} />
                <Route path="portfolio" element={<Portfolio />} />
              </Routes>
              <Footer />
            </Container>
          </BrowserRouter>
        </Body>
    </ThemeProvider>
  );
};

export default Portfolio2020;
