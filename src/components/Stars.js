// thank you Harsh Patel! for the original code : https://codepen.io/zeztron
import React, { useEffect } from "react";
import styled from "styled-components";

const Canvas = styled.canvas`
  height: 100%;
  width: 100%;
  position: fixed;
  top: 0;
  left: 0;
  filter: blur(1px);
  z-index: 0;
`;

const config = {
  perspective: 3,
  star_color: "255, 255, 255",
  starColors: [
    "2254, 227, 238",
    "252, 243, 228",
    "252, 220, 200",
    "200, 224, 207",
    "250, 235, 199",
    "255, 255, 255",
  ],
  speed: 1,
  stars_count: 2,
};

const startStars = (canvas, context) => {
  const width = canvas.offsetWidth;
  const height = canvas.offsetHeight;
  const canvas_center_x = width / 2;
  const canvas_center_y = height / 2;
  const stars_count = width / config.stars_count;
  const focal_length = width / config.perspective;
  const speed = (config.speed * width) / 2000;

  let animation_frame = "";
  let stars = [];

  for (let i = 0; i < stars_count; i++) {
    stars.push({
      x: Math.random() * width,
      y: Math.random() * height,
      z: Math.random() * width,
      color:
        config.starColors[Math.floor(Math.random() * config.starColors.length)],
    });
  }

  window.cancelAnimationFrame(animation_frame);
  canvas.style.opacity = 1;

  let cow = new Image();
  cow.src =
    "https://gallery.yopriceville.com/var/resizes/Free-Clipart-Pictures/Jewelry-and-Diamonds-PNG/Blue_Crystal_PNG_Clipart.png?m=1585819761";
  cow.onload = () => renderStars();

  //-----------------//
  const renderStars = () => {
    animation_frame = window.requestAnimationFrame(function () {
      renderStars();
    });

    context.clearRect(0, 0, width, height);

    for (let i = 0; i < stars.length; i += 1) {
      let star = stars[i];
      star.z -= speed;
      if (star.z <= 0) {
        stars[i] = {
          x: Math.random() * canvas.width,
          y: Math.random() * canvas.height,
          z: canvas.width,
        };
      }

      let star_x =
        (star.x - canvas_center_x) * (focal_length / star.z) + canvas_center_x;
      let star_y =
        (star.y - canvas_center_y) * (focal_length / star.z) + canvas_center_y;
      let star_radius = Math.max(0, (1.4 * (focal_length / star.z)) / 2);
      let star_opacity = 1.2 - star.x / canvas.width;
      let cow_width = Math.max(0.1, (100 * (focal_length / star.z)) / 2);

      if (star.cow) {
        context.save();
        context.translate(
          star_x - cow_width + cow_width / 2,
          star_y - cow_width + cow_width / 2
        );
        context.rotate(star.z / star.rotation_speed);
        context.translate(
          -(star_x - cow_width + cow_width / 2),
          -(star_y - cow_width + cow_width / 2)
        );
        context.globalAlpha = star_opacity;
        context.drawImage(
          cow,
          0,
          0,
          cow.width,
          cow.width,
          star_x - cow_width,
          star_y - cow_width,
          cow_width,
          cow_width
        );
        context.restore();
      } else {
        context.fillStyle = "rgba(" + star.color + "," + star_opacity + ")";
        //context.shadowBlur = 10;
        //context.shadowColor = "pink"; too slow rendering
        context.beginPath();
        context.arc(star_x, star_y, star_radius, 0, Math.PI * 2);
        context.fill();
        context.closePath();
      }
    }
  };
  //-------------------//
};

const Stars = () => {
  const canvasRef = React.useRef(null); // interact with an element of the DOM

  useEffect(() => {
    // can only exist after render of the element (hence useRef)
    let canvas = canvasRef.current;
    let context = canvas.getContext("2d");
    startStars(canvas, context);
  });

  return (
    <Canvas
      ref={canvasRef}
      width={window.innerWidth}
      height={window.innerHeight}
    >
      Stars
    </Canvas>
  );
};

export default Stars;
