import { css } from "styled-components";
import { keyframes } from "styled-components";

export const AniFadeIn = keyframes`
    0% {
        opacity: 0;
        -webkit-transform: scale(.97);
        transform: scale(.97)
    }

    100% {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1)
    }
`;
export const AniFadeOut = keyframes`
    0% {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1)
    }

    100% {
        opacity: 0;
        -webkit-transform: scale(.97);
        transform: scale(.97)
    }
`;
const bg = keyframes`
0%{background-position:0% 75%}
50%{background-position:100% 26%}
100%{background-position:0% 75%}
`;
export const AniBackground = css`
  animation: ${bg} 37s ease infinite;
`;
