import React, { useRef, useEffect } from "react";
import styled from "styled-components";
import { AiFillCloseCircle } from "react-icons/ai";

const ModalContainer = styled.div`
  display: ${(props) => (props.show ? "block" : "none")};
  position: fixed;
  overflow: auto;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  background-color: rgba(0, 0, 0, 0.7);
  transition: all 1s;
  z-index: 999;
  color: #999;
  line-height: 25px;

  & > div {
    background-color: white;
    max-width: 90%;
    box-sizing: border-box;
    border-radius: 5px;
    margin: 3% auto;
    padding: 20px;
    text-align: center;
    display: flex;
    position: relative;
    transition: all 2s;
  }

  & > div > div {
    width: 100%;
    height: 100%;
    position: relative;
    margin-top: 20px;
  }
`;
const ButtonClose = styled.button`
  float: right;
  position: absolute;
  top: 10px;
  right: 10px;
  cursor: pointer;
  font-weight: bold;
  border: none;
  background: none;
`;

const Modal = ({ children, showModal, setShowModal }) => {
  const wrapperRef = useRef(null);
  const useClickOutside = (ref) => {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target) && showModal)
          setShowModal(false);
      }
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [ref, showModal]);
  };
  useClickOutside(wrapperRef);

  return (
    <ModalContainer show={showModal}>
      <div ref={wrapperRef}>
        <ButtonClose onClick={() => setShowModal(false)}>
          <AiFillCloseCircle fontSize="30px" />
        </ButtonClose>
        <div>{children}</div>
      </div>
    </ModalContainer>
  );
};

export default Modal;
