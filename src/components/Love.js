import React from "react";
import styled, { keyframes } from "styled-components";
import { BsHeartFill } from "react-icons/bs";

const love = keyframes`
  to {
    -webkit-ransform: scale(1.1);
    -moz-transform: scale(1.1);
    transform: scale(1.1);
  }
`;
const Heart = styled.div`
  position: relative;
  display: inline-block;
  color: #e01268;
  -webkit-transform: scale(0.9);
  -moz-transform: scale(0.9);
  transform: scale(0.9);
  -webkit-animation: ${love} 0.3s linear infinite alternate-reverse;
  -moz-animation: ${love} 0.3s linear infinite alternate-reverse;
  animation: ${love} 0.3s linear infinite alternate-reverse;
`;

const Love = () => {
  return (
    <Heart>
      <BsHeartFill />
    </Heart>
  );
};

export default Love;
