import styled from "styled-components";

const H2 = styled.h2`
  font-family: ${(props) => props.theme.font2};
  font-size: 18px;
  color: ${(props) => props.theme.subtitle};
  line-height: ${(props) => (props.largepadding ? "40px" : "19px")};
  letter-spacing: ${(props) => props.theme.subtitle_spacing};
  display: inline-block;
  padding-bottom: 10px;
  border-bottom: ${(props) => props.theme.subtitle_border} solid
    ${(props) => props.theme.subtitle};
`;

export default H2;
