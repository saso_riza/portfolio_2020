import React from "react";
import styled from "styled-components";

const StyledRow = styled.div`
  display: flex;
  margin-top: 30px;
  justify-content: space-between;
`;
const StyledColumn = styled.div`
  width: ${(props) =>
    props.width.includes("px") ? props.width : props.width + "%"};
  text-align: left;
`;

const Row = ({ children }) => {
  return <StyledRow>{children}</StyledRow>;
};

const Column = ({ children, width }) => {
  return <StyledColumn width={width}>{children}</StyledColumn>;
};

export { Row, Column };
