import styled from "styled-components";

const Span = styled.span`
  font-family: ${(props) => props.theme.font1};
  color: ${(props) => props.theme.span_color};
  display: block;
  font-size: 14px;
  margin-top: 10px;
`;

export default Span;
