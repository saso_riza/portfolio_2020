import React from "react";
import styled from "styled-components";

const Button = styled.button`
  border: none;
  background: ${(props) => {
    if (props.active)
      return props.theme.mode === "light"
        ? "#aaa"
        : props.theme.mode === "dark"
        ? "#333"
        : props.theme.socialColors[props.index];
    else {
      return "rgba(0, 0, 0, 0.5)";
    }
  }};
  border-radius: 5px;
  color: ${(props) =>
    props.theme.mode === "light" ? "#fff" : props.theme.color};
  padding: 10px 28px;
  margin: 20px 10px;
  font-family: ${(props) => props.theme.font1};
  font-size: 16px;
  cursor: pointer;
  display: inline-block;
  transition: background 0.1s ease-in;

  &:hover {
    background: ${(props) =>
      props.theme.mode === "light"
        ? "#aaa"
        : props.theme.mode === "dark"
        ? "#333"
        : props.theme.socialColors[props.index]};
  }
`;

const ButtonPortfolioType = ({ text, handleClick, index, active }) => {
  return (
    <Button onClick={() => handleClick(text)} index={index} active={active}>
      {text}
    </Button>
  );
};

export default ButtonPortfolioType;
