import React from "react";
import styled from "styled-components";

const Tip = styled.div`
  position: relative;
  display: inline-block;
`;

const TipText = styled.div`
  visibility: hidden;
  width: 120px;
  background-color: ${(props) =>
    props.theme.mode === "light" ? "#aaa" : props.tcolor};
  color: #fff;
  text-align: center;
  padding: 10px 0;
  border-radius: 5px;
  position: absolute;
  z-index: 1;
  top: -70%;
  left: -50%;
  &::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: ${(props) =>
        props.theme.mode === "light" ? "#aaa" : props.tcolor}
      transparent transparent transparent;
  }
  ${Tip}:hover & {
    visibility: visible;
  }
`;

const Tooltip = ({ children, text, tcolor }) => {
  return (
    <Tip>
      <TipText tcolor={tcolor || "black" }>{text}</TipText>
      {children}
    </Tip>
  );
};

export default Tooltip;
