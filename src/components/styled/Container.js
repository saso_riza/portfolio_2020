import styled from "styled-components";

const Container = styled.div`
  display: flex;
  min-height: 100vh;
  height: calc(100% - ${(props) => props.theme.footerheight});
  z-index: 9;
  flex-direction: column;
  justify-content: space-between;
`;
export default Container;
