import styled from "styled-components";

const H1 = styled.h1`
  font-family: ${(props) => props.theme.font1};
  text-transform: ${(props) => props.theme.title_transform};
  font-size: 36px;
  color: ${(props) => props.theme.title_color};
  margin-top: 13px;
  letter-spacing: ${(props) => props.theme.title_spacing};
`;

export default H1;
